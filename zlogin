#!/usr/bin/env zsh
###############################################################################
# Stephen Booth
# .zlogin - executed for each new login shell. Place login banner etc. here.
###############################################################################

################### Login Banner

# Run the daily fortune cookie through cowsay for added 1980s cool
clear
fortune | cowsay
echo

