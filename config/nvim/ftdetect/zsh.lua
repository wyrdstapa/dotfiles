-------------------------------------------------------------------------------
-- Stephen Booth
-- File detection for z-shell files.
-------------------------------------------------------------------------------

------------------- Modules

-- Neovim
local api = vim.api

------------------- Configuration

-- Z-Shell file type detection
api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = { ".zlogin", ".zlogout", ".zshenv", ".zshrc", "zlogin", "zlogout", "zshenv", "zshrc", "*.zsh" },
  command = "setlocal ft=zsh"
})

