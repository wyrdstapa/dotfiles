-------------------------------------------------------------------------------
-- Stephen Booth
-- init.lua - configuration file for neovim. There is a strict sequence to
-- observe here. First, we set the neovim options; we must do this first as
-- they may be used by and / or affect the plugins that we will configure.
-- Next, we load the plugins and configure them. The we map the keyboard
-- shortcuts. And last we load the autocommands.
-------------------------------------------------------------------------------

------------------- Modules

------------------- Caching

-- Experimental lua caching - must be the first instruction
vim.loader.enable()

------------------- Options

-- Set the neovim options first
require("settings")

------------------- Plugin Configuration

-- Load the plugins with lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.uv.fs_stat(lazypath) then
  vim.fn.system({
    "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Load the plugins
require("lazy").setup("plugins", { install = { colorscheme = { "catppuccin" } }, ui = { border = "rounded" } })

------------------- Key Mappings

-- Map the keyboard shortcuts
require("keymappings")

------------------- Autocommands

-- Finally, load the autocommands
require("autocommands")

