-------------------------------------------------------------------------------
-- Stephen Booth
-- Neovim configuration for java files.
-------------------------------------------------------------------------------

------------------- Modules

-- Neovim
local fn = vim.fn
local og = vim.opt_global

------------------- Configuration

-- Set paths and includes
og.path = "." ..
  ",src/main/java/**,src/test/java/**,src/ft/java/**,src/it/java/**" ..
  ",**/src/main/java/**,**/src/test/java/**,**/src/ft/java/**,**/src/it/java/**"
og.include = "^\\s*import"
og.includeexpr = "substitute(v:fname,'\\.','/','g')"

-- Gradle configuration
if fn.filereadable('./gradlew') then
  -- Set the neovim make programme
  og.makeprg = "./gradlew --quiet"
end

