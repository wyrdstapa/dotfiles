-------------------------------------------------------------------------------
-- Stephen Booth
-- settings.lua - configuration settings for neovim options.
-------------------------------------------------------------------------------

------------------- Modules

-- Neovim
local cmd = vim.cmd
local g = vim.g
local go = vim.go
local opt = vim.opt
local wo = vim.wo

------------------- Configuration

-- Character set
go.encoding = "utf-8"

-- General
go.autoread = true
go.backup = false
go.cmdheight = 1
opt.display:append("lastline")
go.fixendofline = true
opt.formatoptions:append("j")
go.hidden = true
g.mapleader = " "
g.maplocalleader = ","
go.mouse = "a"
go.scrolloff = 10
opt.shortmess:append("c")
go.spelllang = "en_gb"
go.timeoutlen = 500
go.updatetime = 300
go.wildmenu = true
go.writebackup = false

-- Editor options
go.autoindent = true
go.backspace = "indent,eol,start"
go.clipboard = "unnamedplus"
opt.complete:remove("i")
go.completeopt = "menu,menuone,noinsert,noselect"
wo.number = true
wo.relativenumber = true
go.showmatch = true
go.smarttab = true

-- File options
cmd.filetype("plugin indent on")
opt.wildignore:append(".git/*")

-- Fold options
wo.foldmethod = "expr"
wo.foldexpr = 'v:lua.vim.treesitter.foldexpr()'
wo.foldenable = false

-- Search options
go.grepprg = "rg --vimgrep"
go.ignorecase = true
go.inccommand = "nosplit"
go.incsearch = true
go.smartcase = true

-- Status line
go.laststatus = 3
go.ruler = true

-- Style options
go.guicursor = "n-v-c-sm:block,i-ci-ve:ver25,r-cr-o:hor20"
go.termguicolors = true
cmd.syntax("enable")

-- Tab options
go.expandtab = true
go.shiftwidth = 4
go.softtabstop = 4
go.tabstop = 4

-- Window options
go.previewheight = 20
go.splitbelow = true
go.splitright = true
go.winheight = 10

