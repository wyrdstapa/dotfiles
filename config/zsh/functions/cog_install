#!/usr/bin/env zsh
###############################################################################
# Stephen Booth
# cog_install - install applications onto the system.
###############################################################################

# Install layered packages
cog_info "*** Installing layered packages ***"
rpm-ostree install cowsay eza fd-find fortune-mod fzf git-delta luajit luarocks pandoc p7zip ripgrep tidy

# Install flatpaks
cog_info "*** Installing flatpaks ***"
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub org.cockpit_project.CockpitClient -y
flatpak install flathub io.dbeaver.DBeaverCommunity -y
flatpak install flathub com.jgraph.drawio.desktop -y
flatpak install flathub com.github.tchx84.Flatseal -y
flatpak install flathub org.gimp.GIMP -y
flatpak install flathub io.gitlab.librewolf-community -y
flatpak install flathub me.proton.Mail -y
flatpak install flathub org.fedoraproject.MediaWriter -y
flatpak install flathub io.mpv.Mpv -y
flatpak install flathub org.kde.okular -y
flatpak install flathub org.onlyoffice.desktopeditors -y
flatpak install flathub me.proton.Pass -y
flatpak install flathub com.protonvpn.www -y
flatpak install flathub io.github.benini.scid -y
flatpak install flathub org.signal.Signal -y
flatpak install flathub com.github.micahflee.torbrowser-launcher -y
flatpak install flathub org.wezfurlong.wezterm -y
flatpak install flathub net.xmind.XMind -y

# Gradle installation
cog_info "*** Installing gradle ***"
sdk install gradle

# Java installation
cog_info "*** Installing java ***"
sdk install java

# Asciidoctor installation
cog_info "*** Installing asciidoctor ***"
sdk install asciidoctorj
local _asciidoctorname="asciidoctor"
local _asciidoctorfile="$COG[BIN_DIR]/$_asciidoctorname"
echo "#!/usr/bin/env zsh\n" > "$_asciidoctorfile" && \
    echo "asciidoctorj \"\$@\"\n" >> "$_asciidoctorfile" && \
    chmod 755 "$_asciidoctorfile"

# Install docker images
cog_info "*** Installing docker images ***"
cog_docker_install

# Install fonts
cog_info "*** Installing fonts ***"
cog_font_install

# Rust installations
cog_info "*** Installing rust applications ***"
cog_rust_platform
cog_rust_install

# Shell plugins
cog_info "*** Installing shell plugins ***"
[[ -n "$(command -v sheldon)" ]] && eval "$(sheldon source)"
fast-theme XDG:catppuccin-mocha

# Node installation
cog_info "*** Installing node applications ***"

nvm install --lts

nvm install-latest-npm

npm install -g neovim

# Initialise neovim
cog_info "*** Initialising neovim ***"
nvim --headless +"Lazy! sync" +qa
nvim --headless +"MasonToolsInstallSync" +qa

# Initialise yazi
cog_info "*** Initialising yazi ***"
ya pack --add yazi-rs/flavors:catppuccin-mocha
ya pack --add yazi-rs/plugins:chmod
ya pack --add yazi-rs/plugins:full-border
ya pack --add imsi32/yatline
ya pack --add imsi32/yatline-catppuccin
ya pack --add imsi32/yatline-githead

# Clone repositories
cog_info "*** Cloning repositories ***"
cog_git_install
# Set up git hooks for the dotfiles repository
local _dir="$(pwd)"
cd $XDG_DATA_HOME/rcm/dotfiles
gitlint install-hook
cd $_dir
# Build and cache bat themes
bat cache --build

# Set up PostgreSQL
cog_info "*** Preparing PostgreSQL ***"

if [[ ! -d $COG[POSTGRES]/pgdata ]]; then
    mkdir -p $COG[POSTGRES]/pgdata
    podman create --name=postgresdb \
        -p 5432:5432 \
        -e POSTGRES_PASSWORD=secret -e PGDATA=/pgdata \
        -v $COG[POSTGRES]/pgdata:/pgdata:Z \
        docker.io/postgres:latest
fi

# Install NetHack
cog_info "*** Installing NetHack ***"

local _nethackname="nethack"
local _nethackfile="$COG[BIN_DIR]/$_nethackname"
podman create --name="$_nethackname" -it docker.io/matsuu/nethack:latest
echo "#!/usr/bin/env zsh\n" > "$_nethackfile" && \
    echo "clear" >> "$_nethackfile" && \
    echo "podman start -a $_nethackname" >> "$_nethackfile" && \
    chmod 755 "$_nethackfile"

# Enable timers
systemctl --user enable trash.timer

