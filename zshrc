#!/usr/bin/env zsh
###############################################################################
# Stephen Booth
# .zshrc - executed for each new interactive shell. Place aliases, functions,
# prompt settings and options here.
###############################################################################

################### Shell Settings

# Set the history
HISTSIZE=1000
SAVEHIST="$HISTSIZE"
HISTFILE="$COG[ZSH_STATE]/history"

setopt appendhistory
setopt incappendhistory
setopt histignorealldups
setopt histreduceblanks

# Set the shell editor mode
bindkey -v

################### Sheldon Plugin Manager

# Load the plugins
[[ -n "$(command -v sheldon)" ]] && eval "$(sheldon source)"

################### Command Line Prompt

# Enable the Starship prompt
[[ -n "$(command -v starship)" ]] && eval "$(starship init zsh)"

################### SdkMan

# Initialise the sdkman environment
[[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh"

################### Completions

# Enable completions
autoload -Uz compinit
compinit -d $COG[ZCOMPDUMP]

################### Functions

autoload -Uz $COG[ZSH_FUNCTIONS]/*(N-.x:t)

################### FZF Fuzzy Finder

# Set up fzf key bindings and fuzzy completion
source <(fzf --zsh)

################### Aliases

# General
alias l="eza --all --icons --long --git --header"
alias librewolf="flatpak run io.gitlab.librewolf-community"
alias lt="eza --all --icons --tree --level 3 --long --git --header"
alias mpv="flatpak run io.mpv.Mpv"
alias wezterm="flatpak run org.wezfurlong.wezterm"
# PostgreSQL
alias pgstart="podman start postgresdb"
alias pgstop="podman stop postgresdb"
alias psql="podman exec -it postgresdb psql -U postgres"
# System
alias comp="$COG[ZSH_FUNCTIONS]/cog_compile"
alias inst="$COG[ZSH_FUNCTIONS]/cog_compile && cog_install"
alias rs="rpm-ostree status"
alias setup="$COG[ZSH_FUNCTIONS]/cog_compile && cog_setup"
alias sp="systemctl poweroff"
alias sr="systemctl reboot"
alias upd="cog_compile && cog_update"
alias yy="cog_yazi"

